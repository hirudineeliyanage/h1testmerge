// This file is used to register all your cloud functions in GCP.
// DO NOT EDIT/DELETE THIS, UNLESS YOU KNOW WHAT YOU ARE DOING.

exports.h1testmergefunction = require("./h1testmerge/function.js").handler;
exports.h1testmergehirupython = require("./h1testmerge/hirupython.py").handler;