let google = require('googleapis').google;
let _auth = require('./Authorizer');
const pubsub = google.pubsub('v1');

exports.handler = function (request, response) {
    pubsub.projects.topics.publish({
        topic: `projects/${process.env.GCP_PROJECT}/topics/testhiru`,
        resource: {
            messages: [{
                data: `this is a test`,
                attributes: {
                    "hiru": "001",
                    "kumu": "01"
                }
            }]
        }
    })
        .then(response => {
            console.log(response.data);           // successful response
            /*
            response.data = {
                "messageIds": [
                    "<numeric-message-id>"
                ]
            }
            */
        })
        .catch(err => {
            console.log(err, err.stack); // an error occurred
        });

    response.send({ "message": "Sample test hiru sample" });
}